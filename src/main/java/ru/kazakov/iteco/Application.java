package ru.kazakov.iteco;

import ru.kazakov.iteco.context.Bootstrap;

public final class Application {

    public static void main(String[] args) throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        new Bootstrap().start();
    }

}
