package ru.kazakov.iteco.service;

import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Task;
import java.util.List;

public final class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    private final ITaskRepository repository;

    public TaskService(final ITaskRepository repository) {this.repository = repository;}

    public String getName(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.getName(id);
    }

    public void setName(final String name, final String id) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        repository.setName(name, id);
    }

    public String getProjectId(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.getProjectId(id);
    }

    public void setProjectId(final String id, final String projectId) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        repository.setProjectId(id, projectId);
    }

    public void remove(final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        repository.remove(ids);
    }

    public void removeAll(final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.removeAll(currentUserId);
    }

    public Task findByName(final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.findByName(name);
    }

    public Task findByName(final String name, final String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findByName(name, currentUserId);
    }

    @Override
    public List<Task> findAll(final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        return repository.findAll(ids);
    }

    public List<Task> findAll(final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findAll(currentUserId);
    }

    public boolean contains(final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.contains(name);
    }

    public boolean contains(final String name, final String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.contains(name, currentUserId);
    }

    public boolean isEmpty(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.isEmpty(id);
    }

    @Override
    protected ITaskRepository getRepository() {return repository;}

}
