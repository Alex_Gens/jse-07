package ru.kazakov.iteco.service;

import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.entity.Project;
import java.util.List;

public final class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    private final IProjectRepository repository;

    public ProjectService(final IProjectRepository projectRepository) {this.repository = projectRepository;}

    public String getName(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.getName(id);
    }

    public void setName(final String name, final String id) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        repository.setName(name, id);
    }

    public void remove(final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        repository.remove(ids);
    }

    public void removeAll(final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.removeAll(currentUserId);
    }

    public Project findByName(final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.findByName(name);
    }

    public Project findByName(final String name, final String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findByName(name, currentUserId);
    }

    public List<Project> findAll(final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        return repository.findAll(ids);
    }

    public List<Project> findAll(final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findAll(currentUserId);
    }

    public boolean contains(final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.contains(name);
    }

    public boolean contains(final String name, final String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.contains(name, currentUserId);
    }

    public boolean isEmpty(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.isEmpty(id);
    }

    @Override
    protected IProjectRepository getRepository() {return repository;}

}
