package ru.kazakov.iteco.service;

import ru.kazakov.iteco.api.repository.IRepository;
import ru.kazakov.iteco.api.service.IService;
import java.util.List;

public abstract class AbstractService<T, E extends IRepository<T>> implements IService<T> {

    public void merge(final T entity) throws Exception {
        if (entity == null) throw new Exception();
        getRepository().merge(entity);
    }

    public void persist(final T entity) throws Exception {
        if (entity == null) throw new Exception();
        getRepository().persist(entity);
    }

    public void remove(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        getRepository().remove(id);
    }

    public void removeAll() {getRepository().removeAll();}

    public T findOne(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return  getRepository().findOne(id);
    }

    public List<T> findAll() {return getRepository().findAll();}

    public boolean isEmpty() {return getRepository().isEmpty();}

    protected abstract E getRepository();

}
