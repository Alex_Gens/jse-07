package ru.kazakov.iteco.service;

import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.User;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IUserRepository repository;

    public UserService(final IUserRepository userRepository) {this.repository = userRepository;}

    public String getName(final String id) {return repository.getName(id);}

    public void setName(final String name, final String id) {repository.setName(name, id);}

    @Override
    public void remove(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        repository.remove(id);
    }

    public User findByLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        return repository.findByLogin(login);
    }

    public boolean contains(final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        return repository.contains(login);
    }

    @Override
    protected IUserRepository getRepository() {return repository;}

}
