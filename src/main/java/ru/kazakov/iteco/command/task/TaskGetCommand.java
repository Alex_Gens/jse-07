package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class TaskGetCommand extends TaskAbstractCommand {

    private final String name = "task-get";

    private final String description = "Show all task information.";

    public TaskGetCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        System.out.println("ENTER TASK NAME: ");
        final String name = ConsoleUtil.enterIgnoreEmpty();
        if (!taskService.contains(name, currentUserId)) {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final Task task = taskService.findByName(name, currentUserId);
        if (taskService.isEmpty(task.getId())) {
            System.out.println("Task is empty. Use \"task-update\" to update this task.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        System.out.println("[Task: " + name + "]");
        System.out.println(task.getInfo());
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
