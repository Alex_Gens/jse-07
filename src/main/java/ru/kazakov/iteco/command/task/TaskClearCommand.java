package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class TaskClearCommand extends TaskAbstractCommand {

    private final String name = "task-clear";

    private final String description = "Remove all tasks.";

    public TaskClearCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        taskService.removeAll(currentUserId);
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println("Tasks successfully removed!");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
