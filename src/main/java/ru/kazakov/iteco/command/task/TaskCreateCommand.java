package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class TaskCreateCommand extends TaskAbstractCommand {

    private final String name = "task-create";

    private final String description = "Create new task.";

    public TaskCreateCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        System.out.println("ENTER TASK NAME: ");
        final String name = ConsoleUtil.enterIgnoreEmpty();
        if (taskService.contains(name, currentUserId)) {
            System.out.println("[NOT CREATED]");
            System.out.println("Project with this name is already exists. Use another project name.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final Task task = new Task(currentUserId);
        task.setName(name);
        taskService.persist(task);
        System.out.println("[CREATED]");
        System.out.println("Task successfully created!");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
