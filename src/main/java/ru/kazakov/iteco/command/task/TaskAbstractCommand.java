package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.command.AbstractCommand;

public abstract class TaskAbstractCommand  extends AbstractCommand {

    protected final ITaskService taskService = serviceLocator.getTaskService();

    protected String currentUserId;

    public TaskAbstractCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    public void execute() throws Exception {
        final String currentUserId = currentState.getCurrentUser().getId();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        this.currentUserId = currentUserId;
    }

}
