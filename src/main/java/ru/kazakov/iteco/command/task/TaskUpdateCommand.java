package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class TaskUpdateCommand extends TaskAbstractCommand {

    private final String name = "task-update";

    private final String description = "Update task information.";

    public TaskUpdateCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        System.out.println("ENTER TASK NAME: ");
        final String name = ConsoleUtil.enterIgnoreEmpty();
        if (!taskService.contains(name, currentUserId)) {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final Task task = taskService.findByName(name, currentUserId);
        System.out.println("Use \"-save\" to finish entering, and save information.");
        System.out.println("ENTER TASK INFORMATION: ");
        final String newInfo = ConsoleUtil.read();
        task.setInfo(newInfo);
        System.out.println("[UPDATED]");
        System.out.println("Task successfully updated!");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
