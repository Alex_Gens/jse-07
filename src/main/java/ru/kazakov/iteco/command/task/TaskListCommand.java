package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.Task;
import java.util.List;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class TaskListCommand extends TaskAbstractCommand {

    private final String name = "task-list";

    private final String description = "Show all tasks.";

    public TaskListCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        final List<Task> tasks = taskService.findAll(currentUserId);
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("Task list is empty. Use \"task-create\" to create task.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        System.out.println("[TASKS LIST]");
        int counter = 1;
        for (Task task : tasks) {
            System.out.println(counter + ". " + task.getName());
            counter++;
        }
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
