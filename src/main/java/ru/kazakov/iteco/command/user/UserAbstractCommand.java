package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.command.AbstractCommand;

public abstract class UserAbstractCommand extends AbstractCommand {

    protected final IUserService userService = serviceLocator.getUserService();

    public UserAbstractCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public abstract String getName();

    @Override
    public abstract String getDescription();

    @Override
    public abstract void execute() throws Exception;

}
