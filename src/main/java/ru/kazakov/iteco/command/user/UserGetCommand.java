package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.ConsoleUtil;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class UserGetCommand extends UserAbstractCommand {

    private final String name = "user-get";

    private final String description = "Show user profile information.";

    public UserGetCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        User user = currentState.getCurrentUser();
        if (user.getRoleType() == RoleType.ADMINISTRATOR) {
            System.out.println("Enter user's login to get user's profile.   [" + user.getRoleType().getName().toUpperCase() + "]");
            System.out.println("ENTER LOGIN: ");
            String login = ConsoleUtil.enterIgnoreEmpty();
            boolean isExist = userService.contains(login);
            if (!isExist) {
                System.out.println("[NOT CORRECT]");
                System.out.println("User with that login doesn't exist.");
                System.out.print(ConsoleUtil.lineSeparator);
                return;
            }
            user = userService.findByLogin(login);
        }
        System.out.println("Profile information: ");
        final String name = user.getName();
        boolean nameIsExist = name != null && !name.isEmpty();
        if (nameIsExist) System.out.println("Name: " + name);
        final String login = user.getLogin();
        final boolean loginIsExist = login != null && !login.isEmpty();
        if (loginIsExist) System.out.println("Login: " + login);
        if (!nameIsExist && !loginIsExist) {
            System.out.println("Profile has no information.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final Date dateStart = user.getDateStart();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        if (dateStart != null) {
            System.out.println("Registration date: " + dateFormat.format(dateStart));
            System.out.print(ConsoleUtil.lineSeparator);
        }
    }

}
