package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.ConsoleUtil;
import java.util.List;

public final class UserListCommand extends  UserAbstractCommand {

    private final String name = "user-list";

    private final String description = "Show all users.   [" + RoleType.ADMINISTRATOR.getName().toUpperCase() + "]";

    public UserListCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
        this.admin = true;
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() {
        final List<User> users = userService.findAll();
        if (users == null || users.isEmpty()) {
            System.out.println("User list is empty.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        System.out.println("[USERS LIST]");
        int counter = 1;
        for (User user : users) {
            System.out.println(counter + ". " + user.getLogin());
            counter++;
        }
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
