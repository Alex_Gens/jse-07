package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class UserLogoutCommand extends UserAbstractCommand {

    private final String name = "user-logout";

    private final String description = "Logout from account.";

    public UserLogoutCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() {
        currentState.setCurrentUser(null);
        System.out.println("[LOGOUT]");
        System.out.println("You logout from your account.");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
