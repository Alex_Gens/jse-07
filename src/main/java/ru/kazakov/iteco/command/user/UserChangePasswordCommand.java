package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.ConsoleUtil;
import ru.kazakov.iteco.util.Password;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public final class UserChangePasswordCommand extends UserAbstractCommand {

    private final String name = "user-change-password";

    private final String description = "Change profile password.";

    public UserChangePasswordCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        final User currentUser = currentState.getCurrentUser();
        User user = currentState.getCurrentUser();
        if (currentUser.getRoleType() == RoleType.ADMINISTRATOR) {
            System.out.println("Enter user's login to change profile password.   [" + user.getRoleType().getName().toUpperCase() + "]");
            System.out.println("ENTER LOGIN: ");
            final String login = ConsoleUtil.enterIgnoreEmpty();
            boolean isExist = userService.contains(login);
            if (!isExist) {
                System.out.println("[NOT CORRECT]");
                System.out.println("User with that login doesn't exist.");
                System.out.print(ConsoleUtil.lineSeparator);
                return;
            }
            user = userService.findByLogin(login);
        }

        if (currentUser.getRoleType() != RoleType.ADMINISTRATOR) {
            checkOldPassword(user);
        }
        String firstPassword = "";
        String secondPassword = "";
        while (true) {
            System.out.println("ENTER NEW PASSWORD: ");
            firstPassword = ConsoleUtil.enterIgnoreEmpty();
            System.out.println("Confirm you password.");
            System.out.println("ENTER NEW PASSWORD: ");
            secondPassword = ConsoleUtil.enterIgnoreEmpty();
            if (!firstPassword.equals(secondPassword)) {
                System.out.println("[NOT UPDATED]");
                System.out.println("Entered passwords are different.");
                System.out.print(ConsoleUtil.lineSeparator);
                continue;
            }
            System.out.println("[UPDATED]");
            break;
        }
        System.out.println("Password updated!");
        System.out.print(ConsoleUtil.lineSeparator);
        final String newPassword = Password.getHashedPassword(firstPassword);
        user.setPassword(newPassword);
    }

    private void checkOldPassword(final User user) throws IOException, NoSuchAlgorithmException {
        System.out.println("ENTER PASSWORD: ");
        final String entered = ConsoleUtil.enterIgnoreEmpty();
        final String enteredPassword = Password.getHashedPassword(entered);
        final String oldPassword = user.getPassword();
        if (!enteredPassword.equals(oldPassword)) {
            System.out.println("[NOT CORRECT]");
            System.out.print(ConsoleUtil.lineSeparator);
        }
    }

}
