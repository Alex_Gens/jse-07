package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.util.ConsoleUtil;
import ru.kazakov.iteco.util.Password;

public final class UserCreateCommand extends UserAbstractCommand {

    private final String name = "user-create";

    private final String description = "New user Registration.";

    public UserCreateCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
        this.secure = false;
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        System.out.println("Registration new profile.");
        System.out.println("ENTER LOGIN ");
        String login = ConsoleUtil.enterIgnoreEmpty();
        if (userService.contains(login)) {
            System.out.println("Login is already exist. Use another login.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        System.out.println("[CORRECT]");
        System.out.print(ConsoleUtil.lineSeparator);
        String firstPassword = "";
        String secondPassword = "";
        while (true) {
            System.out.println("ENTER PASSWORD: ");
            firstPassword = ConsoleUtil.enterIgnoreEmpty();
            System.out.print(ConsoleUtil.lineSeparator);
            System.out.println("Confirm you password.");
            System.out.println("ENTER PASSWORD: ");
            secondPassword = ConsoleUtil.enterIgnoreEmpty();
            if (!firstPassword.equals(secondPassword)) {
                System.out.println("[NOT CORRECT]");
                System.out.println("Entered passwords are different.");
                System.out.print(ConsoleUtil.lineSeparator);
                continue;
            }
            System.out.println("[CORRECT]");
            System.out.println("Profile created!");
            System.out.print(ConsoleUtil.lineSeparator);
            break;
        }
        final User user = new User();
        user.setLogin(login);
        final String password = Password.getHashedPassword(firstPassword);
        user.setPassword(password);
        userService.persist(user);
    }

}
