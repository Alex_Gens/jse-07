package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class UserUpdateCommand extends UserAbstractCommand {

    private final String name = "user-update";

    private final String description = "Update user name in profile";

    public UserUpdateCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        final User currentUser = currentState.getCurrentUser();
        User user = currentState.getCurrentUser();
        if (currentUser.getRoleType() == RoleType.ADMINISTRATOR) {
            System.out.println("Enter user's login to update user's profile.   [" + user.getRoleType().getName().toUpperCase() + "]");
            System.out.println("ENTER LOGIN: ");
            final String login = ConsoleUtil.enterIgnoreEmpty();
            boolean isExist = userService.contains(login);
            if (!isExist) {
                System.out.println("[NOT CORRECT]");
                System.out.println("User with that login doesn't exist.");
                System.out.print(ConsoleUtil.lineSeparator);
                return;
            }
            user = userService.findByLogin(login);
        }
        System.out.println("ENTER NAME: ");
        final String name = ConsoleUtil.enterIgnoreEmpty();
        user.setName(name);
        System.out.println("[UPDATED]");
        System.out.println("Name successfully updated!");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
