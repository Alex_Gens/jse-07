package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.util.ConsoleUtil;
import ru.kazakov.iteco.util.Password;

public final class UserLoginCommand extends UserAbstractCommand{

    private final String name = "user-login";

    private final String description = "User authorization.";

    public UserLoginCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
        this.secure = false;
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER LOGIN: ");
        final String login = ConsoleUtil.enterIgnoreEmpty();
        if (!userService.contains(login)) {
            System.out.println("[NOT CORRECT]");
            System.out.println("Login doesn't exist.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final User currentUser = currentState.getCurrentUser();
        if (currentUser != null &&
                login.equals(currentState.getCurrentUser().getLogin())) {
            System.out.println("[NOT CORRECT]");
            System.out.println("You are already authorized.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        System.out.println("[CORRECT]");
        System.out.print(ConsoleUtil.lineSeparator);
        final User user = userService.findByLogin(login);
        System.out.println("ENTER PASSWORD: ");
        final String password = Password.getHashedPassword(ConsoleUtil.enterIgnoreEmpty());
        if (!user.getPassword().equals(password)) {
            System.out.println("[NOT CORRECT]");
            System.out.println("Incorrect password.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        System.out.println("[CORRECT]");
        currentState.setCurrentUser(user);
        System.out.println("Welcome " + user.getLogin() + "!" + " ["
                + user.getRoleType().getName().toUpperCase() + "]");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
