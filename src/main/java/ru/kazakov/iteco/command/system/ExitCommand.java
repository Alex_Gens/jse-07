package ru.kazakov.iteco.command.system;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    private final String name = "exit";

    private final String description = "Close task manager.";

    public ExitCommand(final ServiceLocator serviceLocator, final CurrentState currentState) {
        super(serviceLocator, currentState);
        this.secure = false;
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() {
        System.out.println("*** MANAGER CLOSED ***");
        System.exit(0);
    }

}
