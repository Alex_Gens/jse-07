package ru.kazakov.iteco.command.system;

import com.jcabi.manifests.Manifests;
import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class AboutCommand extends AbstractCommand {

    private final String name = "about";

    private final String description = "Show information about build.";

    public AboutCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
        this.secure = false;
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() {
        System.out.println("Build number is: " + Manifests.read("buildNumber"));
        System.out.println("Developer: " + Manifests.read("developer"));
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
