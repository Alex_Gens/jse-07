package ru.kazakov.iteco.command.system;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.ConsoleUtil;
import java.util.List;

public final class HelpCommand extends AbstractCommand {

    private final String name = "help";

    private final String description = "Show all commands.";

    public HelpCommand(final ServiceLocator serviceLocator, final CurrentState currentState) {
        super(serviceLocator, currentState);
        this.secure = false;
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() {
        final User currentUser = currentState.getCurrentUser();
        final List<AbstractCommand> commands = currentState.getCommands();
        if (currentUser == null || currentUser.getRoleType() != RoleType.ADMINISTRATOR) {
            commands.forEach(v -> {
                if (!v.isAdmin()){
                    System.out.println(v.getName() + ": " + v.getDescription());
                }});
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        commands.forEach(v -> {System.out.println(v.getName() + ": " + v.getDescription());});
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
