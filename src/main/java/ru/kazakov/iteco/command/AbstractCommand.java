package ru.kazakov.iteco.command;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    protected CurrentState currentState;

    protected boolean secure = true;

    protected boolean admin = false;

    public AbstractCommand(final ServiceLocator serviceLocator, final CurrentState currentState) {
        this.serviceLocator = serviceLocator;
        this.currentState = currentState;
    }

    public abstract String getName();

    public abstract String getDescription();

    public void setServiceLocator(final ServiceLocator serviceLocator) {this.serviceLocator = serviceLocator;};

    public abstract void execute() throws Exception;

    public boolean isSecure() {return secure;};

    public boolean isAdmin() {return admin;}

}
