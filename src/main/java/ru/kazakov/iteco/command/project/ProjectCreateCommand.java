package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class ProjectCreateCommand extends ProjectAbstractCommand {

    private final String name = "project-create";

    private final String description = "Create new project.";

    public ProjectCreateCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        System.out.println("ENTER PROJECT NAME: ");
        final String name = ConsoleUtil.enterIgnoreEmpty();
        if (projectService.contains(name, currentUserId)) {
            System.out.println("[NOT CREATED]");
            System.out.println("Project with this name is already exists. Use another project name.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final  Project project = new Project(currentUserId);
        project.setName(name);
        projectService.persist(project);
        System.out.println("[CREATED]");
        System.out.println("Project successfully created!");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
