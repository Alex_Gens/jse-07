package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.util.ConsoleUtil;
import java.util.ArrayList;
import java.util.List;

public final class ProjectClearCommand extends ProjectAbstractCommand {

    private final String name = "project-clear";

    private final String description = "Remove all projects.";

    public ProjectClearCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        final List<String> taskIds = new ArrayList<>();
        final  List<String> projectIds = new ArrayList<>();
        for (Project project : projectService.findAll(currentUserId)) {
            projectIds.add(project.getId());
        }
        final  ITaskService taskService = serviceLocator.getTaskService();
        for (Task task : taskService.findAll(currentUserId)) {
            if (projectIds.contains(task.getProjectId())) {
                taskIds.add(task.getId());
            }
        }
        taskService.remove(taskIds);
        projectService.remove(projectIds);
        System.out.println("[ALL PROJECTS REMOVED]");
        System.out.println("Projects successfully removed!");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
