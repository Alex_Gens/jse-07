package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.command.AbstractCommand;

public abstract class ProjectAbstractCommand extends AbstractCommand {

    protected final IProjectService projectService = serviceLocator.getProjectService();

    protected String currentUserId;

    public ProjectAbstractCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    public void execute() throws Exception {
        final String currentUserId = currentState.getCurrentUser().getId();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        this.currentUserId = currentUserId;
    }

}
