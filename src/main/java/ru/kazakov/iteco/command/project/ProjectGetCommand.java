package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class ProjectGetCommand extends ProjectAbstractCommand {

    private final String name = "project-get";

    private final String description = "Show all project information.";

    public ProjectGetCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        System.out.println("ENTER PROJECT NAME: ");
        final String name = ConsoleUtil.enterIgnoreEmpty();
        if (!projectService.contains(name, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final Project project = projectService.findByName(name, currentUserId);
        if (projectService.isEmpty(project.getId())) {
            System.out.println("Project is empty. Use \"project-update\" to update this project.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        System.out.println("[Project: " + name + "]");
        System.out.println(project.getInfo());
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
