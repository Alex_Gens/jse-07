package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class ProjectAddTaskCommand extends ProjectAbstractCommand {

    private final String name = "project-add-task";

    private final String description = "Add task to project.";

    public ProjectAddTaskCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        System.out.println("ENTER PROJECT NAME: ");
        final String projectName = ConsoleUtil.enterIgnoreEmpty();
        if (!projectService.contains(projectName, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final Project project = projectService.findByName(projectName,currentUserId );
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("ENTER TASK NAME: ");
        final String taskName = ConsoleUtil.enterIgnoreEmpty();
        if (!taskService.contains(taskName, currentUserId)) {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final Task task = taskService.findByName(taskName,currentUserId);
        if (task.getProjectId().equals(project.getId())) {
            System.out.println("Task is already added. Use project-list-tasks to see tasks in project.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        task.setProjectId(project.getId());
        System.out.println("Task successfully added!");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
