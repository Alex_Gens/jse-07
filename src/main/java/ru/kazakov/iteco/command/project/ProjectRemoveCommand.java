package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.util.ConsoleUtil;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRemoveCommand extends ProjectAbstractCommand {

    private final String name = "project-remove";

    private final String description = "Remove project.";

    public ProjectRemoveCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        System.out.println("ENTER PROJECT NAME: ");
        final String name = ConsoleUtil.enterIgnoreEmpty();
        if (!projectService.contains(name, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final Project project = projectService.findByName(name, currentUserId);
        final ITaskService taskService = serviceLocator.getTaskService();
        final List<String> taskIds = new ArrayList<>();
        for ( Task task : taskService.findAll(currentUserId)
        ) {
            if (task.getProjectId().equals(project.getId())) {
                taskIds.add(task.getId());
            }
        }
        taskService.remove(taskIds);
        projectService.remove(project.getId());
        System.out.println("[REMOVED]");
        System.out.println("Project successfully removed!");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
