package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class ProjectUpdateCommand extends ProjectAbstractCommand {

    private final String name = "project-update";

    private final String description = "Update project information.";

    public ProjectUpdateCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        System.out.println("ENTER PROJECT NAME: ");
        final String name = ConsoleUtil.enterIgnoreEmpty();
        if (!projectService.contains(name, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final Project project = projectService.findByName(name, currentUserId);
        System.out.println("Use \"-save\" to finish entering, and save information.");
        System.out.println("ENTER PROJECT INFORMATION: ");
        final String newInfo = ConsoleUtil.read();
        project.setInfo(newInfo);
        System.out.println("[UPDATED]");
        System.out.println("Project successfully updated!");
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
