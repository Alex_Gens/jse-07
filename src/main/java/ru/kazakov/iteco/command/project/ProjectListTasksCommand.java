package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.util.ConsoleUtil;
import java.util.ArrayList;
import java.util.List;

public final class ProjectListTasksCommand extends ProjectAbstractCommand {

    private final String name = "project-list-tasks";

    private final String description = "Show all tasks in project.";

    public ProjectListTasksCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        System.out.println("ENTER PROJECT NAME: ");
        final String projectName = ConsoleUtil.enterIgnoreEmpty();
        if (!projectService.contains(projectName, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        final ITaskService taskService = serviceLocator.getTaskService();
        final Project project = projectService.findByName(projectName, currentUserId);
        final List<String> taskIds = new ArrayList<>();
        for (Task task : taskService.findAll(currentUserId)
        ) {
            if (task.getProjectId().equals(project.getId())) taskIds.add(task.getId());
        }
        if (taskIds.isEmpty()) {
            System.out.println("The project has no tasks. Use project-add-task to add task to project.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        int counter = 1;
        System.out.println("[TASKS LIST]");
        for (String taskId : taskIds) {
            System.out.println(counter + ": " + taskService.getName(taskId));
            counter++;
        }
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
