package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.Project;
import java.util.List;
import ru.kazakov.iteco.util.ConsoleUtil;

public final class ProjectListCommand extends ProjectAbstractCommand {

    private final String name = "project-list";

    private final String description = "Show all projects.";

    public ProjectListCommand(ServiceLocator serviceLocator, CurrentState currentState) {
        super(serviceLocator, currentState);
    }

    @Override
    public String getName() {return name;}

    @Override
    public String getDescription() {return description;}

    @Override
    public void execute() throws Exception {
        super.execute();
        final List<Project> projects = projectService.findAll(currentUserId);
        if (projects == null || projects.isEmpty()) {
            System.out.println("Project list is empty. Use \"project-create\" to create project.");
            System.out.print(ConsoleUtil.lineSeparator);
            return;
        }
        System.out.println("[PROJECTS LIST]");
        int counter = 1;
        for (Project project : projects) {
            System.out.println(counter + ". " + project.getName());
            counter++;
        }
        System.out.print(ConsoleUtil.lineSeparator);
    }

}
