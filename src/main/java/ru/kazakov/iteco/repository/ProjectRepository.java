package ru.kazakov.iteco.repository;

import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.entity.Project;
import java.util.List;
import java.util.stream.Collectors;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public String getName(final String id) {return entities.get(id).getName();}

    public void setName(final String name, final String id) {entities.get(id).setName(name);}

    @Override
    public void merge(final Project entity) {
        entities.merge(entity.getId(), entity, (v1, v2)
                -> {v1.setInfo(v2.getInfo());
            return v1;});
    }

    @Override
    public void remove(final List<String> ids) {
        entities.entrySet().removeIf(entry
                -> ids.contains(entry.getValue().getId()));
    }

    @Override
    public void removeAll(final String currentUserId) {
        entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(currentUserId));
    }

    @Override
    public Project findByName(final String name) {
        return entities.values().stream()
                .filter(v -> v.getName().equals(name))
                .findFirst().orElse(null);
    }

    @Override
    public Project findByName(final String name, final String currentUserId) {
        return entities.values().stream()
                .filter(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId))
                .findFirst().orElse(null);
    }

    @Override
    public List<Project> findAll(final List<String> ids) {
        return entities.values().stream()
                .filter( v -> ids.contains(v.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Project> findAll(final String currentUserId) {
        return  entities.values().stream()
                .filter( v -> v.getUserId().equals(currentUserId))
                .collect(Collectors.toList());
    }

    @Override
    public boolean contains(final String name) {
        return entities.values().stream()
                .anyMatch(v -> v.getName().equals(name));
    }

    @Override
    public boolean contains(final String name, final String currentUserId) {
        return entities.values().stream()
                .anyMatch(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId));
    }

    @Override
    public boolean isEmpty(final String id) {return entities.get(id).isEmpty();}

}
