package ru.kazakov.iteco.repository;

import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.entity.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public String getName(final String id) {return entities.get(id).getName();}

    @Override
    public void setName(final String name, final String id) {entities.get(id).setName(name);}

    @Override
    public User findByLogin(final String login) {
        return entities.values().stream()
                .filter(v -> v.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    @Override
    public boolean contains(final String login) {
        return entities.values().stream()
                .anyMatch(v -> v.getLogin().equals(login));
    }

}
