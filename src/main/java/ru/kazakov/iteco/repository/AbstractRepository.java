package ru.kazakov.iteco.repository;

import ru.kazakov.iteco.api.repository.IRepository;
import ru.kazakov.iteco.entity.AbstractEntity;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    protected final Map<String, T> entities = new LinkedHashMap<>();

    @Override
    public void merge(final T entity) {entities.merge(entity.getId(), entity, (oldVal, newVal) -> newVal);}

    @Override
    public void persist(final T entity) {entities.putIfAbsent(entity.getId(), entity);}

    @Override
    public void remove(final String id) {entities.remove(id);}

    @Override
    public void removeAll() {entities.clear();}

    @Override
    public T findOne(final String id) {return entities.get(id);}

    @Override
    public List<T> findAll() {return new ArrayList<>(entities.values());}

    @Override
    public boolean isEmpty() {return entities.isEmpty();}

}
