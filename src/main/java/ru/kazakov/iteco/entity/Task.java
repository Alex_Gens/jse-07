package ru.kazakov.iteco.entity;

import java.util.Date;

public final class Task extends AbstractEntity {

    private String userId;

    private String projectId;

    private String name = "";

    private String info = "";

    private Date dateStart = new Date();

    private Date dateFinish = new Date();

    public Task() {}

    public Task(final String userId) {this.userId = userId;}

    public String getName() {return name;}

    public void setName(final String name) {this.name = name;}

    public String getUserId() {return userId;}

    public void setUserId(String userId) {this.userId = userId;}

    public Date getDateStart() {return dateStart;}

    public Date getDateFinish() {return dateFinish;}

    public void setDateStart(final Date dateStart) {this.dateStart = dateStart;}

    public void setDateFinish(final Date dateFinish) {this.dateFinish = dateFinish;}

    public String getProjectId() {return projectId;}

    public void setProjectId(final String projectId) {this.projectId = projectId;}

    public String getInfo() {return info;}

    public void setInfo(final String info) {this.info = info;}

    public boolean isEmpty() {return info == null || info.isEmpty();}

}
