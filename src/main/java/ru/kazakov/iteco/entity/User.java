package ru.kazakov.iteco.entity;

import ru.kazakov.iteco.enumeration.RoleType;
import java.util.Date;

public final class User extends AbstractEntity {

    private String name = "";

    private String login = "";

    private String password = "";

    private Date dateStart = new Date();

    private Date dateFinish = new Date();

    private RoleType roleType = RoleType.DEFAULT;

    public User() {}

    public String getName() {return name;}

    public void setName(final String name) {this.name = name;}

    public Date getDateStart() {return dateStart;}

    public void setDateStart(final Date dateStart) {this.dateStart = dateStart;}

    public Date getDateFinish() {return dateFinish;}

    public void setDateFinish(final Date dateFinish) {this.dateFinish = dateFinish;}

    public String getLogin() {return login;}

    public void setLogin(final String login) {this.login = login;}

    public String getPassword() {return password;}

    public void setPassword(final String password) {this.password = password;}

    public RoleType getRoleType() {return roleType;}

    public void setRoleType(final RoleType roleType) {this.roleType = roleType;}

}
