package ru.kazakov.iteco.enumeration;

public enum RoleType {

    DEFAULT("default"),
    ADMINISTRATOR("Administrator");

    private String displayName = "";

    RoleType(final String displayName) {this.displayName = displayName;}

    public String getName() {return displayName;}

}
