package ru.kazakov.iteco.api.repository;

import ru.kazakov.iteco.entity.Project;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    public String getName(String id);

    public void setName(String name, String id);

    public void remove(List<String> ids);

    public void removeAll(String currentUserId);

    public Project findByName(String name);

    public Project findByName(String name, String currentUserId);

    public List<Project> findAll(List<String> ids);

    public List<Project> findAll(String currentUserId);

    public boolean contains(String name);

    public boolean contains(String name, String currentUserId);

    public boolean isEmpty(String id);

}
