package ru.kazakov.iteco.api.repository;

import ru.kazakov.iteco.entity.Task;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    public String getName(String id);

    public void setName(String name, String id);

    public String getProjectId(String id);

    public void setProjectId(String id, String projectId);

    public void remove(List<String> ids);

    public void removeAll(String currentUserId);

    public Task findByName(String name);

    public Task findByName(String name, String currentUserId);

    public List<Task> findAll(List<String> ids);

    public List<Task> findAll(String currentUserId);

    public boolean contains(String name);

    public boolean contains(String name, String currentUserId);

    public boolean isEmpty(String id);

}
