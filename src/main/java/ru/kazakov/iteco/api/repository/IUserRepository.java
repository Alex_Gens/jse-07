package ru.kazakov.iteco.api.repository;

import ru.kazakov.iteco.entity.User;

public interface IUserRepository extends IRepository<User> {

    public String getName(String id);

    public void setName(String name, String id);

    public User findByLogin(String login) throws Exception;

    public boolean contains(String login) throws Exception;

}
