package ru.kazakov.iteco.api.repository;

import java.util.List;

public interface IRepository<T> {

    public void merge(T entity) throws Exception;

    public void persist(T entity) throws Exception;

    public void remove(String id) throws Exception;

    public void removeAll();

    public T findOne(String id) throws Exception;

    public List<T> findAll();

    public boolean isEmpty();

}
