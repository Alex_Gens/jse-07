package ru.kazakov.iteco.api.context;

import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.entity.User;
import java.util.List;

public interface CurrentState {

    public User getCurrentUser();

    public void setCurrentUser(User currentUser);

    public List<AbstractCommand> getCommands();

}
