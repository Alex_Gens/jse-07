package ru.kazakov.iteco.api.context;

import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.api.service.IUserService;

public interface ServiceLocator {

    public ITaskService getTaskService();

    public IProjectService getProjectService();

    public IUserService getUserService();

}
