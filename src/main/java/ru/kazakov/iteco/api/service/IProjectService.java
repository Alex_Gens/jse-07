package ru.kazakov.iteco.api.service;

import ru.kazakov.iteco.entity.Project;
import java.util.List;

public interface IProjectService extends IService<Project> {

    public String getName(String id) throws Exception;

    public void setName(String name, String id) throws Exception;

    public void remove(List<String> ids) throws Exception;

    public void removeAll(String currentUserId) throws Exception;

    public Project findByName(String name) throws Exception;

    public Project findByName(String name, String currentUserId) throws Exception;

    public List<Project> findAll(List<String> ids) throws Exception;

    public List<Project> findAll(String currentUserId) throws Exception;

    public boolean contains(String name) throws Exception;

    public boolean contains(String name, String currentUserId) throws Exception;

    public boolean isEmpty(String id) throws Exception;

}
