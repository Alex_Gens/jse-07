package ru.kazakov.iteco.api.service;

import java.util.List;

public interface IService<T> {

    public void merge(T entity) throws Exception;

    public void persist(T entity) throws Exception;

    public void remove(String id) throws Exception;

    public void removeAll();

    public T findOne(String id) throws Exception;

    public List<T> findAll();

    public boolean isEmpty();

}
