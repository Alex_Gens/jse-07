package ru.kazakov.iteco.api.service;

import ru.kazakov.iteco.entity.User;

public interface IUserService extends IService<User> {

    public String getName(String id);

    public void setName(String name, String id);

    public User findByLogin(String login) throws Exception;

    public boolean contains(String login) throws Exception;

}
