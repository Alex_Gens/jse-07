package ru.kazakov.iteco.api.service;

import ru.kazakov.iteco.entity.Task;
import java.util.List;

public interface ITaskService extends IService<Task> {

    public String getName(String id) throws Exception;

    public void setName(String name, String id) throws Exception;

    public String getProjectId(String id) throws Exception;

    public void setProjectId(String id, String projectId) throws Exception;

    public void remove(List<String> ids) throws Exception;

    public void removeAll(String currentUserId) throws Exception;

    public Task findByName(String name) throws Exception;

    public Task findByName(String name, String currentUserId) throws Exception;

    public List<Task> findAll(List<String> ids) throws Exception;

    public List<Task> findAll(String currentUserId) throws Exception;

    public boolean contains(String name) throws Exception;

    public boolean contains(String name, String currentUserId) throws Exception;

    public boolean isEmpty(String id) throws Exception;

}
