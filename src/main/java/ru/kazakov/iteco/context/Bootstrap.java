package ru.kazakov.iteco.context;

import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.command.*;
import ru.kazakov.iteco.command.project.*;
import ru.kazakov.iteco.command.system.*;
import ru.kazakov.iteco.command.task.*;
import ru.kazakov.iteco.command.user.*;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.repository.*;
import ru.kazakov.iteco.service.*;
import ru.kazakov.iteco.util.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator, CurrentState {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IUserRepository userRepository = new UserRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IUserService userService = new UserService(userRepository);
    private final ServiceLocator serviceLocator = this;
    private final CurrentState currentState = this;
    private User currentUser = null;

    {
        registry(new ProjectCreateCommand(serviceLocator, currentState));
        registry(new ProjectGetCommand(serviceLocator, currentState));
        registry(new ProjectUpdateCommand(serviceLocator, currentState));
        registry(new ProjectRemoveCommand(serviceLocator, currentState));
        registry(new ProjectListCommand(serviceLocator, currentState));
        registry(new ProjectAddTaskCommand(serviceLocator, currentState));
        registry(new ProjectListTasksCommand(serviceLocator, currentState));
        registry(new ProjectClearCommand(serviceLocator, currentState));
        registry(new TaskCreateCommand(serviceLocator, currentState));
        registry(new TaskGetCommand(serviceLocator, currentState));
        registry(new TaskUpdateCommand(serviceLocator, currentState));
        registry(new TaskRemoveCommand(serviceLocator, currentState));
        registry(new TaskListCommand(serviceLocator, currentState));
        registry(new TaskClearCommand(serviceLocator, currentState));
        registry(new UserLoginCommand(serviceLocator, currentState));
        registry(new UserChangePasswordCommand(serviceLocator, currentState));
        registry(new UserLogoutCommand(serviceLocator, currentState));
        registry(new UserGetCommand(serviceLocator, currentState));
        registry(new UserCreateCommand(serviceLocator, currentState));
        registry(new UserUpdateCommand(serviceLocator, currentState));
        registry(new UserListCommand(serviceLocator, currentState));
        registry(new HelpCommand(serviceLocator, currentState));
        registry(new ExitCommand(serviceLocator, currentState));
        registry(new AboutCommand(serviceLocator, currentState));
    }

    public void start() throws Exception {
        addUsers();
        String command = "";
        while (true) {
            if (currentUser == null) {
                System.out.println("You are not authorized. Use \"user-login\" for authorization.");
                System.out.print(ConsoleUtil.lineSeparator);
            }
            command = ConsoleUtil.enterIgnoreEmpty();
            command = command.trim().toLowerCase();
            execute(command);
        }
    }

    @Override
    public ITaskService getTaskService() {return taskService;}

    @Override
    public IProjectService getProjectService() {return projectService;}

    @Override
    public IUserService getUserService() {return userService;}

    @Override
    public User getCurrentUser() {return currentUser;}

    @Override
    public void setCurrentUser(final User currentUser) {this.currentUser = currentUser;}

    @Override
    public List<AbstractCommand> getCommands() {return new ArrayList<>(commands.values());}

    private void registry(final AbstractCommand command) {commands.put(command.getName(), command);}

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        if (!commands.containsKey(command)) {
            System.out.println("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            return;
        }
        final AbstractCommand abstractCommand = commands.get(command);
        if (currentUser == null && abstractCommand.isAdmin()) {
            System.out.println("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            return;
        }
        if (currentUser == null && abstractCommand.isSecure()) {
            System.out.println("[NO ACCESS]");
            return;
        }
        if (currentUser == null && !abstractCommand.isSecure()) {
            abstractCommand.execute();
            return;
        }
        if (currentUser.getRoleType() != RoleType.ADMINISTRATOR && abstractCommand.isAdmin()) {
            System.out.println("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            return;
        }
        abstractCommand.execute();
    }

    private void addUsers() throws Exception {
        final User user = new User();
        user.setLogin("user");
        final String password = Password.getHashedPassword("pass");
        user.setPassword(password);
        user.setRoleType(RoleType.DEFAULT);
        final User admin = new User();
        admin.setLogin("admin");
        admin.setPassword(password);
        admin.setRoleType(RoleType.ADMINISTRATOR);
        userService.persist(user);
        userService.persist(admin);
    }

}
