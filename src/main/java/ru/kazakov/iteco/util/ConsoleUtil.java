package ru.kazakov.iteco.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class ConsoleUtil {

    public static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static final String lineSeparator = System.lineSeparator();

    public static String enterIgnoreEmpty() throws IOException {
        String name = reader.readLine();
        while (name == null || name.isEmpty()) name = reader.readLine();
        return name;
    }

    public static String read() throws IOException {
        final StringBuilder builder = new StringBuilder().append("");
        String temp = reader.readLine();
        while (!temp.equals("-save")) {
            builder.append(temp);
            builder.append(lineSeparator);
            temp = reader.readLine();
        }
        return builder.toString();
    }

}
