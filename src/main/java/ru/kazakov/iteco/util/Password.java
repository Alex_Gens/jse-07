package ru.kazakov.iteco.util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Password {

    public static String getHashedPassword(final String password) throws NoSuchAlgorithmException {
        final String salt = "@JF27$o%";
        String hashed = password;
        for (int i = 0; i < 60000 ; i++) {
            hashed = salt + hashed + salt;
            hashed = md5Custom(hashed);
        }
        return hashed;
    }

    private static String md5Custom(final String password) throws NoSuchAlgorithmException {
        final MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        final byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }

}
